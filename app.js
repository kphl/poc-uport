const express = require('express');
const mustacheExpress = require('mustache-express');
const Web3 = require('web3');
const jsrsasign = require('jsrsasign');
const EthrDID = require('ethr-did');
const bodyParser = require("body-parser");
const DidRegistryContract = require('ethr-did-registry');
const Contract = require('truffle-contract');

var app = express();


// Configs
app.engine('html', mustacheExpress());
app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Choosing provider
if (typeof(web3) !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("https://rinkeby.infura.io/v3/7cb9d1e5b8f44156bfeb1d8498a19399"));
}

// Version
var version = web3.version;
console.log("Web3.js version " + version);

// Creating client account
var clientAccount = web3.eth.accounts.create();
var clientAddress = clientAccount.address;
var clientPrivateKey = clientAccount.privateKey;
var clientPublicKey = clientAccount.publicKey;
var clientDid = new EthrDID({address: clientAddress, privateKey: clientPrivateKey, provider: web3.currentProvider});

// Creating user account
var userAccount = web3.eth.accounts.create();
var userAddress = userAccount.address;
var userPrivateKey = userAccount.privateKey;
var userPublicKey = userAccount.publicKey;
var userDid = new EthrDID({address: userAddress, privateKey: userPrivateKey, provider: web3.currentProvider});


var jsonInterface = [
	{
		"constant": false,
		"inputs": [
			{
				"name": "identity",
				"type": "address"
			},
			{
				"name": "name",
				"type": "bytes32"
			},
			{
				"name": "value",
				"type": "bytes"
			}
		],
		"name": "revokeAttribute",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"name": "owners",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "address"
			},
			{
				"name": "",
				"type": "bytes32"
			},
			{
				"name": "",
				"type": "address"
			}
		],
		"name": "delegates",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "identity",
				"type": "address"
			},
			{
				"name": "sigV",
				"type": "uint8"
			},
			{
				"name": "sigR",
				"type": "bytes32"
			},
			{
				"name": "sigS",
				"type": "bytes32"
			},
			{
				"name": "name",
				"type": "bytes32"
			},
			{
				"name": "value",
				"type": "bytes"
			},
			{
				"name": "validity",
				"type": "uint256"
			}
		],
		"name": "setAttributeSigned",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "identity",
				"type": "address"
			},
			{
				"name": "sigV",
				"type": "uint8"
			},
			{
				"name": "sigR",
				"type": "bytes32"
			},
			{
				"name": "sigS",
				"type": "bytes32"
			},
			{
				"name": "newOwner",
				"type": "address"
			}
		],
		"name": "changeOwnerSigned",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "identity",
				"type": "address"
			},
			{
				"name": "delegateType",
				"type": "bytes32"
			},
			{
				"name": "delegate",
				"type": "address"
			}
		],
		"name": "validDelegate",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"name": "nonce",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "identity",
				"type": "address"
			},
			{
				"name": "name",
				"type": "bytes32"
			},
			{
				"name": "value",
				"type": "bytes"
			},
			{
				"name": "validity",
				"type": "uint256"
			}
		],
		"name": "setAttribute",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "identity",
				"type": "address"
			},
			{
				"name": "delegateType",
				"type": "bytes32"
			},
			{
				"name": "delegate",
				"type": "address"
			}
		],
		"name": "revokeDelegate",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "identity",
				"type": "address"
			}
		],
		"name": "identityOwner",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "identity",
				"type": "address"
			},
			{
				"name": "sigV",
				"type": "uint8"
			},
			{
				"name": "sigR",
				"type": "bytes32"
			},
			{
				"name": "sigS",
				"type": "bytes32"
			},
			{
				"name": "delegateType",
				"type": "bytes32"
			},
			{
				"name": "delegate",
				"type": "address"
			}
		],
		"name": "revokeDelegateSigned",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "identity",
				"type": "address"
			},
			{
				"name": "sigV",
				"type": "uint8"
			},
			{
				"name": "sigR",
				"type": "bytes32"
			},
			{
				"name": "sigS",
				"type": "bytes32"
			},
			{
				"name": "delegateType",
				"type": "bytes32"
			},
			{
				"name": "delegate",
				"type": "address"
			},
			{
				"name": "validity",
				"type": "uint256"
			}
		],
		"name": "addDelegateSigned",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "identity",
				"type": "address"
			},
			{
				"name": "delegateType",
				"type": "bytes32"
			},
			{
				"name": "delegate",
				"type": "address"
			},
			{
				"name": "validity",
				"type": "uint256"
			}
		],
		"name": "addDelegate",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "identity",
				"type": "address"
			},
			{
				"name": "sigV",
				"type": "uint8"
			},
			{
				"name": "sigR",
				"type": "bytes32"
			},
			{
				"name": "sigS",
				"type": "bytes32"
			},
			{
				"name": "name",
				"type": "bytes32"
			},
			{
				"name": "value",
				"type": "bytes"
			}
		],
		"name": "revokeAttributeSigned",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "identity",
				"type": "address"
			},
			{
				"name": "newOwner",
				"type": "address"
			}
		],
		"name": "changeOwner",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"name": "changed",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "identity",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "owner",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "previousChange",
				"type": "uint256"
			}
		],
		"name": "DIDOwnerChanged",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "identity",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "delegateType",
				"type": "bytes32"
			},
			{
				"indexed": false,
				"name": "delegate",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "validTo",
				"type": "uint256"
			},
			{
				"indexed": false,
				"name": "previousChange",
				"type": "uint256"
			}
		],
		"name": "DIDDelegateChanged",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "identity",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "name",
				"type": "bytes32"
			},
			{
				"indexed": false,
				"name": "value",
				"type": "bytes"
			},
			{
				"indexed": false,
				"name": "validTo",
				"type": "uint256"
			},
			{
				"indexed": false,
				"name": "previousChange",
				"type": "uint256"
			}
		],
		"name": "DIDAttributeChanged",
		"type": "event"
	}
];

var EthereumDidReg = new web3.eth.Contract(jsonInterface);
console.log(EthereumDidReg.methods);

// https://developer.uport.me/categories/ethr-did-registry/#contract-deployments


// Generating JWT for Selective Disclosure Request

// Header
var oHeader = {alg: 'HS256', typ: 'JWT'};
var sHeader = JSON.stringify(oHeader);

// Payload
var oPayload = {
    iss: clientDid.did,
    type: 'shareReq',
    iat: jsrsasign.KJUR.jws.IntDate.get('now'),
    exp: jsrsasign.KJUR.jws.IntDate.get('now + 1day'),
    callback:"localhost:3000/client",
};
var sPayload = JSON.stringify(oPayload);

// Json Web Token
var signedJWT = jsrsasign.KJUR.jws.JWS.sign('HS256', sHeader, sPayload, clientPrivateKey);

// Generating JWT for Selective Disclosure Response
var oRespHeader = {alg: 'HS256', typ:'JWT'};
var sRespHeader = JSON.stringify(oRespHeader);
var oRespPayload = {
    iss: userDid.did,
    type: "shareResp",
    aud: clientDid.did,
    iat: jsrsasign.KJUR.jws.IntDate.get('now'),
    express: jsrsasign.KJUR.jws.IntDate.get('now + 1day'),
    req: signedJWT
};
var sRespPayload = JSON.stringify(oRespPayload);

// Json Web Token Response
var signedJWTResp = jsrsasign.KJUR.jws.JWS.sign('HS256', sRespHeader, sRespPayload, userPrivateKey);


// Mustache template
app.get('/client', function(req, res) {
    res.render('client.html', 
        {
            "ClientAddress": clientAddress,
            "ClientPrivateKey": clientPrivateKey,
            "ClientDid": clientDid.did,
            "UserAddress": userAddress,
            "UserPrivateKey": userPrivateKey,
            "UserDid": userDid.did,
            "JWTRequest": signedJWT,
            "jwtHeaderRequest": JSON.stringify(oHeader, undefined, 2),
            "jwtPayloadRequest": JSON.stringify(oPayload, undefined, 2)
        }
    );
});

app.get('/owner', function(req, res) {
    res.render('owner.html', 
        {
            "UserAddress": userAddress,
            "UserPublicKey": userPublicKey,
            "UserPrivateKey": userPrivateKey,
            "UserDid": userDid.did,
            "JWTRequest": signedJWT,
            "JWTResponse": signedJWTResp,
            "jwtHeaderResponse": JSON.stringify(oRespHeader, undefined, 2),
            "jwtPayloadResponse": JSON.stringify(oRespPayload, undefined, 2)
        }
    );
});
   

app.post('/thank',function(req,res){
    var reply = '';
    reply += "Your user name is " + req.body.user;
    reply += ", password is " + req.body.password;
    reply += ". Hidden value: " + req.body.accept;
    res.send(reply);
    console.log("User name = " + req.body.user + ", password is "+ req.body.password);
});

// Setting the server port
var port = Number(process.env.PORT || 3000);
app.listen(port, function() {
    console.log("Listening on port " + port + "...");
});

